package org.example.jdbc;

import org.example.jdbc.dao.EmployeeDAO;
import org.example.jdbc.model.Employee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class JdbcMain {

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/esercizio", "root", "root")) {

            EmployeeDAO employeeDAO = new EmployeeDAO(connection);

//            Employee toAdd = new Employee();
//            toAdd.setFirstName("Mario");
//            toAdd.setLastName("Azzurri");
//            toAdd.setDateOfBirth(new Date(88944317000L));
//            employeeDAO.addEmployee(toAdd, 1L);
//
//            System.out.println("===============================");

            final List<Employee> employees = employeeDAO.findEmployeesByName("Mario");
            employees.forEach(System.out::println);

            System.out.println("===============================");

            final List<Employee> employees2 = employeeDAO.findEmployeesByDepartmentName("HR");
            employees2.forEach(System.out::println);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}
