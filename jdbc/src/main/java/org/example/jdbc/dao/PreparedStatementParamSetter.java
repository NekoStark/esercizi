package org.example.jdbc.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface PreparedStatementParamSetter {

    void setParams(PreparedStatement preparedStatement) throws SQLException;

}
