package org.example.jdbc.dao;

import org.example.jdbc.model.Employee;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public class EmployeeDAO {

    private final Connection connection;

    public EmployeeDAO(Connection connection) {
        this.connection = connection;
    }

    public List<Employee> findEmployeesByName(String name) throws SQLException {
        return findEmployees(
                "SELECT * FROM employee WHERE first_name = ?",
                ps -> ps.setString(1, name)
        );
    }

    public List<Employee> findEmployeesByDepartmentName(String departmentName) throws SQLException {
        return findEmployees(
                "SELECT e.* FROM employee e LEFT JOIN department d ON e.department_id = d.id WHERE d.name = ?",
                ps -> ps.setString(1, departmentName)
        );
    }

    public List<Employee> findEmployees(String query, PreparedStatementParamSetter paramSetter) throws SQLException {
        List<Employee> result = new LinkedList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            paramSetter.setParams(preparedStatement);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    final Employee employee = mapEmployee(resultSet);
                    result.add(employee);
                }

            }
        }

        return result;
    }

    public void addEmployee(Employee employee, Long departmentId) throws SQLException {
        try (final PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO employee(first_name, last_name, date_of_birth) " +
                        "VALUE (?, ?, ?)"
        )) {

            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getLastName());
            preparedStatement.setDate(3, employee.getDateOfBirth());
            preparedStatement.setLong(4, departmentId);

            final int modifiedRows = preparedStatement.executeUpdate();
            System.out.println("Inserted " + modifiedRows + " rows");
        }

    }

    private static Employee mapEmployee(ResultSet resultSet) throws SQLException {
        Employee result = new Employee();
        result.setId(resultSet.getLong("id"));
        result.setFirstName(resultSet.getString("first_name"));
        result.setLastName(resultSet.getString("last_name"));
        result.setDateOfBirth(resultSet.getDate("date_of_birth"));

        return result;
    }

}
