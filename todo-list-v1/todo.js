function handleLoadComplete() {
  const form = document.getElementById("new_todo_form");
  const input = document.querySelector("#new_todo_form > input");
  const btn = document.querySelector("#new_todo_form > button");

  const list = document.getElementById("todo_list");

  input.onchange = function (e) {
    if (e.target.value) {
      btn.removeAttribute("disabled");
    } else {
      btn.setAttribute("disabled", "disabled");
    }
  };

  form.onsubmit = function (e) {
    e.preventDefault();

    if (!input.value) {
      return;
    }

    const span = document.createElement("span");
    span.textContent = input.value;

    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.onclick = function(e) {
      if (e.target.checked) {
        span.classList.add("completed");
      } else {
        span.classList.remove("completed");
      }
    }

    const div = document.createElement("div");
    div.appendChild(checkbox);
    div.appendChild(span);

    list.appendChild(div);

    input.value = "";
  };
}
