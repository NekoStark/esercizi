# esercizi

### java

[Try with Resource](https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html)

### jdbc / jpa

[Documentazione Oracle JDBC](https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html)

[SQL Injection](https://it.wikipedia.org/wiki/SQL_injection)

[Paradigm Mismatch (e altro...)](https://what-when-how.com/hibernate/understanding-objectrelational-persistence-hibernate/)

### javascript

[Introduzione agli eventi in javascript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events)

[Fetch - utilizzi comuni](https://devhints.io/js-fetch)

[Promise](https://web.archive.org/web/20190521025632/http://kosamari.com/notes/the-promise-of-a-burger-party)

[Fake REST service (hosted)](https://jsonplaceholder.typicode.com/)

[Fake REST service (local)](https://github.com/typicode/json-server)
