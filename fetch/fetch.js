function handleLoadComplete() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then(function (res) {
      return res.json();
    })
    .then(function (users) {
      renderUserList(users);
    });
}

function renderUserList(users) {
  const list = document.querySelector("#user_list > tbody");
  list.innerHTML = "";

  users.forEach((user) => {
    const row = document.createElement("tr");
    const nameCell = document.createElement("td");
    const emailCell = document.createElement("td");

    nameCell.innerText = user.name;
    emailCell.innerText = user.email;

    row.appendChild(emailCell);
    row.appendChild(nameCell);

    list.appendChild(row);
    fetch("https://eu.ui-avatars.com/api/?name=" + user.name)
      .then(res => res.blob())
      .then(blob => {
        const img = document.createElement("img");
        img.src = window.URL.createObjectURL(blob);
        row.appendChild(img);
      });
  });
}
