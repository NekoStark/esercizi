package org.example.collections;

import org.example.collections.service.TextService;

import java.util.List;
import java.util.Map;

public class WordCountMain {

    public static void main(String[] args) {
        String s = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex orci, ullamcorper sit amet luctus et, sagittis ac diam. Nullam rhoncus rhoncus sem, non eleifend felis. Phasellus ut erat ipsum. Nam volutpat vitae tellus aliquam blandit. In a vehicula ex. Sed lobortis nulla sit amet finibus vestibulum. Integer malesuada lectus vitae leo bibendum, fringilla facilisis sapien pellentesque. Etiam luctus pulvinar volutpat. Sed commodo, nisl in pretium faucibus, nulla nibh finibus ante, ut consectetur orci diam quis sapien. Nullam fringilla, odio at venenatis scelerisque, lectus elit tempor eros, vel sagittis lorem orci et leo. Cras vitae metus condimentum, mollis nibh et, tempor quam. Vestibulum vitae neque ut eros consequat pulvinar. Cras nec gravida quam. Nunc at accumsan augue, in consequat ipsum. Vivamus venenatis risus et quam posuere, et hendrerit urna malesuada.";
        String[] split = s.split("\\s");

        TextService service = new TextService();
        List<String> sanitized = service.sanitizeInput(split);
        Map<String, Integer> countMap = service.countOccurrences(sanitized);

        System.out.println(countMap);

        // es. ordinare le parole estratte per ordine alfabetico
        // es. provare ad estrarre il termine con più occorrenze
    }

}
