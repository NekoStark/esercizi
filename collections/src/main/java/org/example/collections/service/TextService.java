package org.example.collections.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TextService {

    public List<String> sanitizeInput(String[] input) {
        List<String> result = new LinkedList<>();

        for (String s : input) {
            if (s == null) {
                continue;
            }
            result.add(
                    s.replace(".", "")
                            .replace(",", "")
                            .trim()
            );
        }

        return result;
    }

    public Map<String, Integer> countOccurrences(List<String> input) {
        Map<String, Integer> result = new HashMap<>();

        for (String s : input) {
            Integer count = result.getOrDefault(s, 0);
            result.put(s, count + 1);
        }

        return result;
    }

}
