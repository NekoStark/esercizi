package org.example.collections;

import org.example.collections.model.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ListExampleMain {

    public static void main(String[] args) {
        List<User> list = new LinkedList<>();

        list.add(new User(2, "giovanni"));
        list.add(new User(1, "simone"));
        list.add(new User(3, "luca"));

        // stampo in ordine di inserimento
        System.out.println(list);

        // ordino per ordinamento "naturale" (per id, definito da User)
        Collections.sort(list);
        System.out.println(list);

        // ordino per username (usando un criterio esterno)
        Collections.sort(list, Comparator.comparing(User::getUsername));
        System.out.println(list);
    }

}
