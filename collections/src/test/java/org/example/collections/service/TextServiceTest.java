package org.example.collections.service;

import org.example.collections.service.TextService;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TextServiceTest {

    private TextService textService;

    @Before
    public void setUp() {
        textService = new TextService();
    }

    @Test
    public void testSanitizeInput() {
        String[] input = {"ciao", " miao ", "bau,", ".pio", null};

        List<String> result = textService.sanitizeInput(input);

        assertEquals(4, result.size());
        assertEquals("ciao", result.get(0));
        assertEquals("miao", result.get(1));
        assertEquals("bau", result.get(2));
        assertEquals("pio", result.get(3));
    }

    @Test
    public void testCountOccurrences() {
        List<String> input = Arrays.asList("ciao", "ciao", "miao", "bau");

        Map<String, Integer> result = textService.countOccurrences(input);

        assertEquals(3, result.size());
        assertEquals(2, result.get("ciao").intValue());
        assertEquals(1, result.get("miao").intValue());
        assertEquals(1, result.get("bau").intValue());
    }

}
