const todos = [{ text: "TODO 1", checked: false }];

function createDomElement(todo) {
  const span = document.createElement("span");
  span.textContent = todo.text;
  if (todo.checked) {
    span.classList.add("completed");
  }

  const checkbox = document.createElement("input");
  checkbox.type = "checkbox";
  checkbox.checked = todo.checked;
  checkbox.onchange = function (e) {
    todo.checked = e.target.checked;
    renderList();
  };

  const div = document.createElement("div");
  div.appendChild(checkbox);
  div.appendChild(span);

  return div;
}

function renderList() {
  const list = document.getElementById("todo_list");

  list.innerHTML = "";

  todos.forEach((t) => {
    list.appendChild(createDomElement(t));
  });
}

function handleLoadComplete() {
  renderList();

  const form = document.getElementById("new_todo_form");
  form.onsubmit = function (e) {
    e.preventDefault();
    const input = document.querySelector("#new_todo_form > input");
    if (!input.value) {
      return;
    }

    todos.push({ text: input.value, checked: false });
    input.value = "";

    renderList();
  };
}
